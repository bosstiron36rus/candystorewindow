﻿using System.Linq;
using System.Threading.Tasks;
using CandyStoreAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CandyStoreAPI.Controllers
{
    /// <summary>
    /// The controller to manage the user sign-in/sign-up
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Produces("application/json")]
    public class AccountController : Controller
    {
        /// <summary>
        /// The user manager
        /// </summary>
        private readonly UserManager<User> userManager;
        /// <summary>
        /// The sign in manager
        /// </summary>
        private readonly SignInManager<User> signInManager;

        /// <summary>
        /// The logger
        /// </summary>
        private readonly ILogger<AccountController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="userManager">The user manager.</param>
        /// <param name="signInManager">The sign in manager.</param>
        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, ILogger<AccountController> logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.logger = logger;
        }

        /// <summary>
        /// Registers the user
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Account/SignIn")]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User
                {
                    Email = model.Email,
                    UserName = model.Email,
                };
                // Добавление нового пользователя
                var result = await this.userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await this.userManager.AddToRoleAsync(user, "user");
                    await this.signInManager.SignInAsync(await this.userManager.FindByEmailAsync(model.Email), false);
                    var msg = new
                    {
                        message = "Добавлен новый пользователь: " + user.UserName,
                        id = await this.userManager.GetUserIdAsync(user),
                        user = model.Email,
                        roles = await userManager.GetRolesAsync(await this.userManager.FindByEmailAsync(model.Email))
                    };
                    this.logger.LogInformation($"Пользователь {user.Email} был успешно зарегистрирован");
                    return Ok(msg);
                }
                else
                {
                    this.logger.LogInformation($"Ну удалось зарегистрировать пользователя {user.Email}");
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    var errorMsg = new
                    {
                        message = "Пользователь не добавлен.",
                        error = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage))
                    };
                    return Ok(errorMsg);
                }
            }
            else
            {
                var errorMsg = new
                {
                    message = "Неверные входные данные.",
                    error = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage))
                };
                this.logger.LogInformation($"Не удалось зарегистрировать пользователя, неверные входные данные");
                return Ok(errorMsg);
            }
        }

        /// <summary>
        /// Logins the user
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Account/LogIn")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await this.signInManager.PasswordSignInAsync(model.Email, model.Password, false, true);
                if (result.Succeeded)
                {
                    this.logger.LogInformation($"Вход пользователем {model.Email} выполнен успешно");
                    var user = await this.userManager.FindByEmailAsync(model.Email);
                    var msg = new
                    {
                        message = "Выполнен вход пользователем: " + model.Email,
                        id = await this.userManager.GetUserIdAsync(user),
                        user = model.Email,
                        roles = await this.userManager.GetRolesAsync(user)
                    };
                    return Ok(msg);
                }
                else
                {
                    this.logger.LogInformation($"Не удалось выполнить вход для пользователя {model.Email}, неверные логин или пароль");
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                    var errorMsg = new
                    {
                        message = "Вход не выполнен.",
                        error = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage))
                    };
                    return Ok(errorMsg);
                }
            }
            else
            {               
                var errorMsg = new
                {
                    message = "Вход не выполнен.",
                    error = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage))
                };
                this.logger.LogInformation($"Не удалось выполнить вход, неверные входные данные");
                return Ok(errorMsg);
            }
        }

        /// <summary>
        /// Logs off the user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Account/LogOff")]
        public async Task<IActionResult> LogOff()
        {
            await this.signInManager.SignOutAsync();
            var msg = new
            {
                message = "Выполнен выход."
            };
            this.logger.LogInformation($"Выполнен выход пользователем");
            return Ok(msg);
        }
    }
}