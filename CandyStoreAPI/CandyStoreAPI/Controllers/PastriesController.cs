﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CandyStoreAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CandyStoreAPI.Controllers
{
    /// <summary>
    /// The controller for CRUD operations with pastries db table.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class PastriesController : ControllerBase
    {
        /// <summary>
        /// The database context
        /// </summary>
        private readonly CandyStoreContext dbContext;
        /// <summary>
        /// The logger
        /// </summary>
        private readonly ILogger<PastriesController> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="PastriesController"/> class.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        /// <param name="logger">The logger.</param>
        public PastriesController(CandyStoreContext dbContext, ILogger<PastriesController> logger)
        {
            this.dbContext = dbContext;
            this.logger = logger;
        }

        // GET: api/pastries
        /// <summary>
        /// Gets all pastries
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Pastry> GetAll()
        {
            return this.dbContext.Pastries.Include(p => p.PastryType).Include(p => p.FeedBacks);
        }

        // GET: api/pastries/5
        /// <summary>
        /// Gets the specific pastry.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetPastry")]
        public IActionResult GetPastry(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Pastry pastry = this.dbContext.Pastries.Include(p => p.PastryType).Include(p => p.FeedBacks).FirstOrDefault(p => p.ID == id);

            if (pastry == null)
            {
                this.logger.LogInformation($"Не найдено кондитерское изделие с ID = {id}");
                return NotFound();
            }

            return Ok(pastry);
        }

        // POST: api/pastries
        /// <summary>
        /// Adds the specified pastry into the db.
        /// </summary>
        /// <param name="pastry">The pastry.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Pastry pastry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            this.dbContext.Pastries.Add(pastry);

            try
            {
                await this.dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                this.logger.LogError(e.Message, e);
                return BadRequest(e.Message);
            }

            this.logger.LogInformation($"Кондитерское изделие '{pastry.ID}' было успешно добавлено");
            return CreatedAtAction("GetPastry", new { id = pastry.ID }, pastry);
        }


        // PUT: api/pastries/1/feedback/deleteBody
        /// <summary>
        /// Deletes the feed back body from pastries feedback, that created by user with id = <param name="id"></param>.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="feedBack">The feed back.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}/feedback/deleteBody")]
        public async Task<IActionResult> DeleteFeedBackBody(int id, [FromBody] FeedBack feedBack)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Pastry pastry = this.dbContext.Pastries.Include(p => p.PastryType).Include(p => p.FeedBacks).FirstOrDefault(p => p.ID == id);

            if (pastry == null)
            {
                this.logger.LogInformation($"Не удалось найти кондитерское изделие с ID = {id}");
                return NotFound();
            }

            var currentFeedBack = pastry.FeedBacks.FirstOrDefault(fb => fb.User_ID == feedBack.User_ID);

            if (currentFeedBack != null && !string.IsNullOrEmpty(currentFeedBack.Body))
            {
                currentFeedBack.Body = null;

                this.dbContext.Update(currentFeedBack);
                try
                {
                    await this.dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    this.logger.LogError(e.Message, e);
                    return BadRequest(e.Message);
                }
            }
            this.logger.LogInformation($"Отзыв пользователя '{feedBack.User_ID}' был успешно удален");
            return CreatedAtAction("GetPastry", new { id = pastry.ID }, pastry);
        }

        // PUT: api/pastries/1/feedback
        /// <summary>
        /// Adds the feedback to pastry.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="feedBack">The feed back.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}/feedback")]
        public async Task<IActionResult> LeftFeedBack(int id, [FromBody] FeedBack feedBack)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Pastry pastry = this.dbContext.Pastries.Include(p => p.PastryType).Include(p => p.FeedBacks).FirstOrDefault(p => p.ID == id);

            if (pastry == null)
            {
                return BadRequest();
            }

            var currentFeedBack = pastry.FeedBacks.FirstOrDefault(fb => fb.User_ID == feedBack.User_ID);

            if (currentFeedBack != null)
            {
                if (string.IsNullOrEmpty(currentFeedBack.Body) && !string.IsNullOrEmpty(feedBack.Body))
                {
                    currentFeedBack.Body = feedBack.Body;
                }
                else
                {
                    currentFeedBack.Vote = feedBack.Vote;
                }
                this.dbContext.Update(currentFeedBack);
            }
            else
            {
                pastry.FeedBacks.Add(feedBack);
                this.dbContext.Update(pastry);
            }

            try
            {
                await this.dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                this.logger.LogError(e.Message, e);
                return BadRequest(e.Message);
            }

            this.logger.LogInformation($"Отзыв '{feedBack.ID}' успешно оставлен");
            return CreatedAtAction("GetPastry", new { id = pastry.ID }, pastry);
        }

        // PUT: api/pastries/5
        /// <summary>
        /// Updates the pastry.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="pastry">The pastry.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Pastry pastry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Pastry inMemoryPastry = this.dbContext.Pastries.Find(id);

            if (inMemoryPastry == null)
            {
                this.logger.LogInformation($"Не найдено кондитерское изделие с ID = {id}");
                return BadRequest();
            }

            inMemoryPastry.Name = pastry.Name;
            inMemoryPastry.ManufactureDate = pastry.ManufactureDate;
            inMemoryPastry.PastryType_ID = pastry.PastryType_ID;
            inMemoryPastry.Price = pastry.Price;
            inMemoryPastry.ShelfLife = pastry.ShelfLife;
            inMemoryPastry.Quantity = pastry.Quantity;

            this.dbContext.Update(inMemoryPastry);
            try
            {
                await this.dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                this.logger.LogError(e.Message, e);
                return BadRequest(e.Message);
            }

            this.logger.LogInformation($"Успешно обновлено кондитерское изделие c ID = {id}");
            return Ok();
        }

        // DELETE: api/pastries/5
        /// <summary>
        /// Deletes the specified pastry.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Pastry pastry = this.dbContext.Pastries.Find(id);

            if (pastry == null)
            {
                this.logger.LogInformation($"Не найдено кондитерское изделие с ID = {id}");
                return NotFound();

            }

            this.dbContext.Pastries.Remove(pastry);
            try
            {
                await this.dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                this.logger.LogError(e.Message, e);
                return BadRequest(e.Message);
            }

            this.logger.LogInformation($"Успешно удалено кондитерское изделие c ID = {id}");
            return Ok();
        }

        // GET: api/pastries/types
        /// <summary>
        /// Gets all pastry types.
        /// </summary>
        /// <returns></returns>
        [HttpGet("types")]
        public IEnumerable<PastryType> GetTypes()
        {
            return this.dbContext.PastryTypes;
        }
    }
}
