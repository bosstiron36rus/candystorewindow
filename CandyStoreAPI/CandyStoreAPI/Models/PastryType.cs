namespace CandyStoreAPI.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The model for pastry type
    /// </summary>
    [Table("PastryType")]
    public partial class PastryType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PastryType"/> class.
        /// </summary>
        public PastryType()
        {
            Pastries = new HashSet<Pastry>();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the pastries.
        /// </summary>
        /// <value>
        /// The pastries.
        /// </value>
        public virtual ICollection<Pastry> Pastries { get; set; }
    }
}
