﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using Microsoft.Extensions.Logging;

namespace CandyStoreAPI.Models
{
    /// <summary>
    /// The database context to work with DB
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityDbContext{CandyStoreAPI.Models.User}" />
    public class CandyStoreContext : IdentityDbContext<User>
    {
        /// <summary>
        /// The logger
        /// </summary>
        private readonly ILogger<CandyStoreContext> logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="CandyStoreContext"/> class.
        /// </summary>
        /// <param name="options">The options to be used by a <see cref="T:Microsoft.EntityFrameworkCore.DbContext" />.</param>
        public CandyStoreContext(DbContextOptions options, ILogger<CandyStoreContext> logger) : base(options)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Gets or sets the pastries.
        /// </summary>
        /// <value>
        /// The pastries.
        /// </value>
        public virtual DbSet<Pastry> Pastries { get; set; }
        /// <summary>
        /// Gets or sets the pastry types.
        /// </summary>
        /// <value>
        /// The pastry types.
        /// </value>
        public virtual DbSet<PastryType> PastryTypes { get; set; }
        /// <summary>
        /// Gets or sets the feed backs.
        /// </summary>
        /// <value>
        /// The feed backs.
        /// </value>
        public virtual DbSet<FeedBack> FeedBacks { get; set; }

        /// <summary>
        /// Configure the database
        /// </summary>
        /// <param name="modelBuilder">The builder being used to construct the model for this context. Databases (and other extensions) typically
        /// define extension methods on this object that allow you to configure aspects of the model that are specific to a given database.
        /// </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            try
            {
                DBInitializer.Initialize(this);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, "An error occurred creating the DB.");
            }

            modelBuilder.Entity<PastryType>().HasMany(e => e.Pastries);
        }
    }
}
