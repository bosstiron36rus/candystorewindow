﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CandyStoreAPI.Models
{
    /// <summary>
    /// The pastry feedback
    /// </summary>
    [Table("FeedBack")]
    public partial class FeedBack
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        /// <value>
        /// The body.
        /// </value>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the vote.
        /// </summary>
        /// <value>
        /// The vote.
        /// </value>
        public bool? Vote { get; set; }

        /// <summary>
        /// Gets or sets the pastry identifier.
        /// </summary>
        /// <value>
        /// The pastry identifier.
        /// </value>
        public int Pastry_ID { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public string User_ID { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        [ForeignKey("User_ID")]
        public virtual User User { get; set; }

        /// <summary>
        /// Gets or sets the pastry.
        /// </summary>
        /// <value>
        /// The pastry.
        /// </value>
        [ForeignKey("Pastry_ID")]
        public virtual Pastry Pastry { get; set; }
    }
}
