namespace CandyStoreAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The model for pastry
    /// </summary>
    [Table("Pastry")]
    public partial class Pastry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pastry" /> class.
        /// </summary>
        public Pastry()
        {
            FeedBacks = new HashSet<FeedBack>();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the shelf life.
        /// </summary>
        /// <value>
        /// The shelf life.
        /// </value>
        public double ShelfLife { get; set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public double Price { get; set; }

        /// <summary>
        /// Gets or sets the pastry type identifier.
        /// </summary>
        /// <value>
        /// The pastry type identifier.
        /// </value>
        public int PastryType_ID { get; set; }

        /// <summary>
        /// Gets or sets the manufacture date.
        /// </summary>
        /// <value>
        /// The manufacture date.
        /// </value>
        public DateTime ManufactureDate { get; set; }

        /// <summary>
        /// Gets or sets the feed backs.
        /// </summary>
        /// <value>
        /// The feed backs.
        /// </value>
        public virtual ICollection<FeedBack> FeedBacks { get; set; }

        /// <summary>
        /// Gets or sets the type of the pastry.
        /// </summary>
        /// <value>
        /// The type of the pastry.
        /// </value>
        [ForeignKey("PastryType_ID")]
        public virtual PastryType PastryType { get; set; }
    }
}
