﻿using Microsoft.AspNetCore.Identity;

namespace CandyStoreAPI.Models
{
    /// <summary>
    /// The user to sign-in/sign-up
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.IdentityUser" />
    public class User : IdentityUser
    {
    }
}