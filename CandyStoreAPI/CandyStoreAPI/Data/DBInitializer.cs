﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CandyStoreAPI.Models
{
    /// <summary>
    /// Data base Initializer
    /// </summary>
    public static class DBInitializer
    {
        /// <summary>
        /// Initializes the new database.
        /// </summary>
        /// <param name="dbContext">The database context.</param>
        public static void Initialize(CandyStoreContext dbContext)
        {
            dbContext.Database.EnsureCreated();
            if (dbContext.Pastries.Any())
            {
                return;
            }

            var pastryTypes = new List<PastryType>
            {
                new PastryType()
                {
                    Name = "Торт",
                },
                new PastryType()
                {
                    Name = "Конфеты",
                }
            };
            pastryTypes.ForEach(pt => dbContext.Add(pt));
            dbContext.SaveChanges();

            var pastries = new List<Pastry>
            {
                new Pastry()
                {
                    Name = "Медовик",
                    Quantity = 10,
                    ShelfLife = 7,
                    Price = 150,
                    PastryType_ID = 1,
                    ManufactureDate = DateTime.Today
                },
                new Pastry()
                {
                    Name= "Лёвушка",
                    Quantity = 80,
                    ShelfLife = 30,
                    Price = 5,
                    PastryType_ID = 2,
                    ManufactureDate = DateTime.Today
                },
            };
            pastries.ForEach(p => dbContext.Add(p));
            dbContext.SaveChanges();
        }
    }
}
