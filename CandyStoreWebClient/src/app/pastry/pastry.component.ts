import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pastry } from '../Models/Pastry';
import { User } from '../Models/User';

@Component({
  selector: 'app-pastry',
  templateUrl: './pastry.component.html',
  styleUrls: ['./pastry.component.css']
})

export class PastryComponent implements OnInit {

  @Input() pastry;
  feedback = '';
  pastryDate = '';
  likes = 0;
  dislikes = 0;
  vote = null;
  hasFeedBack = false;
  showFeedBacks = true;
  showFeedBackView = false;
  selectedPastry = new Pastry();
  authUser: User;
  isAdmin: boolean;

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChanged = new EventEmitter<any>();
  @Output() editEvent = new EventEmitter<any>();
  @Output() reloadEvent = new EventEmitter<any>();

  constructor(public http: HttpClient) {
  }

  ngOnInit() {

    const user = localStorage.getItem('authUser');
    if (user !== null) {
      this.authUser = JSON.parse(user);
      this.isAdmin = this.authUser.Roles.includes('admin', 0);
    } else {
      this.isAdmin = false;
    }

    this.likes = 0;
    this.dislikes = 0;
    this.hasFeedBack = false;

    this.http
      .get(`/api/pastries/${this.pastry.id}`)
      .subscribe((pastry) => this.pastry = pastry);

    this.pastryDate = new Date(Date.parse(this.pastry.manufactureDate)).toLocaleDateString();
    for (const i in this.pastry.feedBacks) {
      if (this.pastry.feedBacks.hasOwnProperty(i)) {
        if (this.pastry.feedBacks[i].vote === true) {
          this.likes++;
        }
        if (this.pastry.feedBacks[i].vote === false) {
          this.dislikes++;
        }
        if (this.pastry.feedBacks[i].user_ID === this.authUser.Id) {
          this.vote = this.pastry.feedBacks[i].vote;
          if (this.pastry.feedBacks[i].body != null) {
            this.hasFeedBack = true;
          }
        }
      }
    }
  }

  makeOrder() {
  }

  like() {
    if (this.vote === true) {
      this.vote = null;
    } else {
      this.vote = true;
    }
    const params = {Vote: this.vote, User_ID: this.authUser.Id };
    this.http.put(`/api/pastries/${this.pastry.id}/feedback`, params).subscribe(() => this.reloadEvent.next(this.pastry));
  }

  dislike() {
    if (this.vote === false) {
      this.vote = null;
    } else {
      this.vote = false;
    }
    const params = {Vote: this.vote, User_ID: this.authUser.Id};
    this.http.put(`/api/pastries/${this.pastry.id}/feedback`, params).subscribe(() => this.reloadEvent.next(this.pastry));
  }

  sendFeedback() {
    const params = {Body: this.feedback, User_ID: this.authUser.Id};
    this.http.put(`/api/pastries/${this.pastry.id}/feedback`, params ).subscribe(() => {
      this.reloadEvent.next(this.pastry);
      this.feedback = '';
    });
    this.showFeedBackView = false;
  }

  deleteFeedBackBody() {
    for (const i in this.pastry.feedBacks) {
      if (this.pastry.feedBacks.hasOwnProperty(i)) {
        if (this.pastry.feedBacks[i].user_ID === this.authUser.Id) {
          this.http
            .put(`/api/pastries/${this.pastry.id}/feedback/deleteBody`, this.pastry.feedBacks[i])
            .subscribe(() => this.reloadEvent.next(this.pastry));
        }
      }
    }
  }

  delete() {
    this.onChanged.emit(this.pastry);
    this.http.delete(`/api/pastries/${this.pastry.id}/`).subscribe(data => console.log(data));
  }

  edit() {
    this.editEvent.next(this.pastry);
  }

  selectPastry() {
    let feedBacksHtml = '<br\>';
    if (this.showFeedBacks) {
      for (const index in this.pastry.feedBacks) {
        if (this.pastry.feedBacks.hasOwnProperty(index) && this.pastry.feedBacks[index].body != null) {
          if (this.pastry.feedBacks[index].user_ID === this.authUser.Id) {
            feedBacksHtml += '<div class=\'card-text font-weight-bold\'>' + this.pastry.feedBacks[index].body + '</div>';
            } else {
            feedBacksHtml += '<div class=\'card-text\'>' + this.pastry.feedBacks[index].body + '</div>';
          }
        }
      }
    }
    this.showFeedBacks = !this.showFeedBacks;
    document.getElementById('feedBacksMain-' + this.pastry.id).innerHTML = feedBacksHtml;
  }

  sendFeedBackView() {
    this.showFeedBackView = !this.showFeedBackView;
  }
}
