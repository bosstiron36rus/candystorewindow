import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public http: HttpClient) { }

  ngOnInit() {
    if (localStorage.length > 0) {
      const tokenExiresTime = localStorage.getItem('expires');
      const currentTime = new Date().getTime().toString();
      if (currentTime > tokenExiresTime) {
        this.http
          .post(`/api/account/logOff`, '')
          .subscribe((response: any) => {
            localStorage.clear();
            location.replace('/');
          });
      }
    }
  }

}
