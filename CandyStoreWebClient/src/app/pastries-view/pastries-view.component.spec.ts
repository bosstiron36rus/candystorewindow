import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastriesViewComponent } from './pastries-view.component';

describe('PastriesViewComponent', () => {
  let component: PastriesViewComponent;
  let fixture: ComponentFixture<PastriesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastriesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastriesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
