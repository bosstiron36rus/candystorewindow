import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Pastry } from '../Models/Pastry';
import { User } from '../Models/User';

@Component({
  selector: 'app-pastries-view',
  templateUrl: './pastries-view.component.html',
  styleUrls: ['./pastries-view.component.css']
})
export class PastriesViewComponent implements OnInit {
  pastries: any;
  pastryTypes: any;
  newPastry = new Pastry();
  editPastry = new Pastry();
  authUser: User;
  isAdmin: boolean;

  constructor(public http: HttpClient) {}

  ngOnInit() {
    if (localStorage.length > 0) {
      const tokenExiresTime = localStorage.getItem('expires');
      const currentTime = new Date().getTime().toString();
      if (currentTime > tokenExiresTime) {
        this.http
          .post(`/api/account/logOff`, '')
          .subscribe((response: any) => {
            localStorage.clear();
            location.replace('/');
          });
      }
    }
    const user = localStorage.getItem('authUser');
    if (user !== null) {
      this.authUser = JSON.parse(user);
      this.isAdmin = this.authUser.Roles.includes('admin', 0);
    }
    this.http
      .get('/api/pastries')
      .pipe(map(data => <any>data))
      .subscribe(pastries => {
          this.pastries = pastries;
      });
    this.http
      .get('/api/pastries/types')
      .pipe(map(data => <any>data))
      .subscribe(types => {
          this.pastryTypes = types;
      });
  }

  HandleOptionChange(e) {
    this.newPastry.PastryType_ID = e.target.value;
  }


  CreatePastry() {
    const selectedTypeIndex = (<HTMLSelectElement>document.getElementById('typeSelector')).selectedIndex;
    const selectedType = this.pastryTypes[selectedTypeIndex].id;
    this.newPastry.PastryType_ID = selectedType;

    this.http
    .post('/api/pastries', this.newPastry)
    .subscribe(() => {
      this.ngOnInit();
      this.newPastry = new Pastry();
    });
  }

  EditPastry(pastry: Pastry) {
    this.editPastry = pastry;
    document.getElementById('editButton').click();
  }

  edit() {
    const selectedTypeIndex = (<HTMLSelectElement>document.getElementById('editTypeSelector')).selectedIndex;
    const selectedType = this.pastryTypes[selectedTypeIndex].id;
    this.editPastry.PastryType_ID = selectedType;

    this.http
    .put('/api/pastries/' + this.editPastry.ID, this.editPastry)
    .subscribe(() => this.ngOnInit());
  }

  onDelete(del: any) {
    const element = this.pastries.find(
      function (currentElement) {
      return currentElement.id === del.id;
    });
    this.pastries.splice(this.pastries.indexOf(element), 1);
  }

  reload() {
    this.ngOnInit();
  }
}
