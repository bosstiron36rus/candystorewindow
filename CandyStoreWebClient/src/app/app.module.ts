import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { PastryComponent } from './pastry/pastry.component';
import {Routes, RouterModule} from '@angular/router';
import { PastriesViewComponent } from './pastries-view/pastries-view.component';
import { OrdersViewComponent } from './orders-view/orders-view.component';
import { OrderComponent } from './order/order.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { SigninComponent } from './signin/signin.component';
import { HomeComponent } from './home/home.component';


const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'pastries', component: PastriesViewComponent},
  { path: 'orders', component: OrdersViewComponent},
  { path: 'logIn', component: LoginComponent},
  { path: 'signIn', component: SigninComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    PastryComponent,
    PastriesViewComponent,
    OrdersViewComponent,
    OrderComponent,
    LoginComponent,
    SigninComponent,
    HomeComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    CommonModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
