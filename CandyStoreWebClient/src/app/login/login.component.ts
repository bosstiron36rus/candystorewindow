import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../Models/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public http: HttpClient) { }

  ngOnInit() {
    localStorage.clear();
  }


  logIn() {
    const email = (<HTMLInputElement>document.getElementById('Email')).value;
    const password = (<HTMLInputElement>document.getElementById('Password')).value;
    document.getElementById('msg').innerHTML = '';
    const formError = document.getElementById('formError');

    while (formError.firstChild) {
      formError.removeChild(formError.firstChild);
    }

    const data = { email: email, password: password };

    this.http
      .post(`/api/account/logIn`, data, {withCredentials: true})
      .subscribe((response) => this.parseResponse(response));
  }

  parseResponse(response) {
    if (response !== '') {
      document.getElementById('msg').innerHTML = response.message;

      if (typeof response.error !== 'undefined' && response.error.length > 0) {
        for (let i = 0; i < response.error.length; i++) {
          const formError = document.getElementById('formError');
          const li = document.createElement('li');
          li.appendChild(document.createTextNode(response.error[i]));
          formError.appendChild(li);
        }
      } else {
        const user = new User(<string>response.id, <string>response.user, <string[]>response.roles);
        localStorage.setItem('authUser', JSON.stringify(user));
        const expires = new Date().setMinutes(new Date().getMinutes() + 10);
        localStorage.setItem('expires', expires.toString());
        location.replace('/');
      }
      (<HTMLInputElement>document.getElementById('Password')).value = '';
    }
  }
}
