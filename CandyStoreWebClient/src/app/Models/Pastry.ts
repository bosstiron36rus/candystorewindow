import { FeedBack } from './FeedBack';

export class Pastry {
    ID: number;
    Name: string;
    ShelfLife: number;
    Quantity: number;
    Price: number;
    PastryType_ID: number;
    ManufactureDate: Date;
    FeedBacks: FeedBack[];
}
