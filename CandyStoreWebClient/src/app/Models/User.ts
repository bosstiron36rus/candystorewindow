export class User {
    Id: string;
    Email: string;
    Roles: string[];

    constructor(id: string, email: string, roles: string[]) {
        this.Id = id;
        this.Email = email;
        this.Roles = roles;
    }
}
