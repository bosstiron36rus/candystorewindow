import { Component } from '@angular/core';
import { User } from './Models/User';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  isSignedIn: boolean;
  authUser: User;

  constructor(public http: HttpClient) { }

  ngOnInit() {
    const user = localStorage.getItem('authUser');
    if (user !== null) {
      this.authUser = JSON.parse(user);
      this.isSignedIn = true;
    } else {
      this.isSignedIn = false;
    }
  }

  logOff() {
    this.http
      .post(`/api/account/logOff`, '')
      .subscribe((response: any) => {
        localStorage.clear();
        location.replace('/');
      });
  }
}
