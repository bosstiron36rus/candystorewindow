import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../Models/User';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['../login/login.component.css']
})
export class SigninComponent implements OnInit {
  pastry: any;

  constructor(public http: HttpClient) { }

  ngOnInit() {
  }

  signIn() {
    const email = (<HTMLInputElement>document.querySelector('#email')).value;
    const password = (<HTMLInputElement>document.querySelector('#password')).value;
    const passwordConfirm = (<HTMLInputElement>document.querySelector('#passwordConfirm')).value;

    const data = { email: email, password: password, passwordConfirm: passwordConfirm };

    this.http
      .post(`/api/account/signIn`, data)
      .subscribe((response) => this.parseResponse(response));
  }

  parseResponse(response) {
    document.querySelector('#msg').innerHTML = '';
    const formError = document.querySelector('#formError');
    while (formError.firstChild) {
      formError.removeChild(formError.firstChild);
    }
    document.querySelector('#msg').innerHTML = response.message;

    if (response.error !== undefined && response.error.length > 0) {
      for (let i = 0; i < response.error.length; i++) {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(response.error[i]));
        formError.appendChild(li);
      }
    } else {
      const user = new User(<string>response.id, <string>response.user, <string[]>response.roles);
      localStorage.setItem('authUser', JSON.stringify(user));
      location.replace('/');
    }
    (<HTMLInputElement>document.querySelector('#password')).value = '';
    (<HTMLInputElement>document.querySelector('#passwordConfirm')).value = '';
  }
}
